package com.db;

import com.db.exceptions.TextLoadException;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Text loader class tests
 *
 * @author Evgeny Sadikov
 */
public class TextLoaderTest {
    private static final String testFileName = "testText.txt";
    private static final String wrongFileName = "wrongFile.txt";

    @Test
    public void shouldReadTextWhenFileExists() throws TextLoadException {
        List<String> sut = TextLoader.loadFromFile(testFileName);

        assertTrue(sut.containsAll(Arrays.asList("Hello", "this", "is", "a", "simple", "text")));
    }

    @Test(expected = TextLoadException.class)
    public void shouldThrowWhenFileNotExist() throws TextLoadException {
        List<String> sut = TextLoader.loadFromFile(wrongFileName);

        assertTrue(sut.isEmpty());
    }
}
