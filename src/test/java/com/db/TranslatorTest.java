package com.db;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Translator class class tests
 *
 * @author Evgeny Sadikov
 */
public class TranslatorTest {
    private static final String word = "word";
    private static final String translation = "translation";

    @Test
    public void shouldTranslateWhenWordIsKnown() {
        Translator sut = new Translator();
        Dictionary dict = new Dictionary();
        List<String> text = new ArrayList<>();

        text.add(word);
        dict.addTranslation(word, translation);

        List<String> res = sut.translateText(text, dict);

        assertTrue(res.contains(translation));
    }

    @Test
    public void shouldReplaceWordWhenWordIsUnknown() {
        Translator sut = new Translator();
        Dictionary dict = new Dictionary();
        List<String> text = new ArrayList<>();

        text.add(word);

        List<String> res = sut.translateText(text, dict);

        assertFalse(res.contains(word));
    }
}
