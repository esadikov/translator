package com.db;

import com.db.exceptions.IncorrectDictionaryFileException;
import com.db.exceptions.NoTranslationException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Dictionary loader class tests
 *
 * @author Evgeny Sadikov
 */
public class DictionaryLoaderTest {
    private static final String testFileName = "testDict.txt";
    private static final String wrongFileName = "wrongFile.txt";
    private static final String word = "word";
    private static final String translation = "слово";


    @Test
    public void shouldReadDictWhenFileCorrect() throws IncorrectDictionaryFileException, NoTranslationException {
        Dictionary sut = DictionaryLoader.loadFromFile(testFileName);

        assertEquals(translation, sut.getTranslation(word));
    }

    @Test(expected = IncorrectDictionaryFileException.class)
    public void shouldThrowWhenFileNotExist() throws IncorrectDictionaryFileException, NoTranslationException {
        Dictionary sut = DictionaryLoader.loadFromFile(wrongFileName);

        assertNotEquals(translation, sut.getTranslation(word));
    }
}
