package com.db;

import org.junit.Test;
import static org.junit.Assert.*;

import com.db.exceptions.NoTranslationException;

/**
 * Dictionary class tests
 *
 * @author Evgeny Sadikov
 */
public class DictionaryTest {
    private static final String word = "word";
    private static final String translation = "translation";
    private static final String anotherWord = "anotherWord";

    @Test
    public void shouldFindTranslationWhenWordIsInDictionary() throws NoTranslationException {
        Dictionary sut = new Dictionary();

        sut.addTranslation(word, translation);

        assertTrue(translation.equals(sut.getTranslation(word)));
    }

    @Test(expected = NoTranslationException.class)
    public void shouldThrowExceptionWhenWordNotIsNotInDictionary() throws NoTranslationException {
        Dictionary sut = new Dictionary();

        sut.addTranslation(word, translation);

        assertFalse(translation.equals(sut.getTranslation(anotherWord)));
    }

}
