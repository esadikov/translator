package com.db;

import com.db.exceptions.IncorrectDictionaryFileException;
import com.db.exceptions.TextLoadException;
import com.db.exceptions.TranslationSaveException;

import java.util.List;

/**
 * Dictionary class.
 *
 * @author Evgeny Sadikov
 */
public class App
{
    private static final String dictionaryFile = "dict.txt";
    private static final String textFile = "text.txt";
    private static final String translationFile = "translation.txt";

    public static void main( String[] args )
    {
        try {
            Dictionary dict = DictionaryLoader.loadFromFile(dictionaryFile);
            List<String> text = TextLoader.loadFromFile(textFile);
            List<String> translation = new Translator().translateText(text, dict);
            TranslationWriter.writeTranslation(translation, translationFile);
            System.out.println("Done!");
        } catch (TextLoadException e) {
            System.out.println("Can't load text");
        } catch (IncorrectDictionaryFileException e) {
            System.out.println("Can't load dictionary");
        } catch (TranslationSaveException e) {
            System.out.println("Can't save translation");
        }
    }
}
