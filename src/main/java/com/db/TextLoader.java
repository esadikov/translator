package com.db;

import com.db.exceptions.TextLoadException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Text loader class.
 *
 * @author Evgeny Sadikov
 */
public class TextLoader {

    /**
     * Load the text to translate from a file.
     *
     * @param fileName name of the text
     * @return List of words from the text
     * @throws TextLoadException in case text file is incorrect
     * @throws NullPointerException if any of parameters is null
     */
    public static List<String> loadFromFile(String fileName) throws TextLoadException {
        Objects.requireNonNull(fileName);

        List<String> text = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"))) {
            String line;
            String[] words;
            while ( (line = reader.readLine()) != null) {
                words = line.split("\\W");
                for (String w : words) {
                    if (w.length() > 0) {
                        text.add(w);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw new TextLoadException("No such file", e);
        } catch (IOException e) {
            throw new TextLoadException("IO error", e);
        }

        return text;
    }

}
