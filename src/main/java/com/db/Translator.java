package com.db;

import com.db.exceptions.NoTranslationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Translator class.
 *
 * @author Evgeny Sadikov
 */
public class Translator {
    private static final String unknownWordReplacement = "*UNKNOWN*";

    /**
     * Translates text using dictionary
     *
     * @param text text to translate
     * @param dict dictionary
     * @return translated text
     *
     * @throws NullPointerException in case any parameter is null
     */
    public List<String> translateText(List<String> text, Dictionary dict) {
        Objects.requireNonNull(text);
        Objects.requireNonNull(dict);

        List<String> translation = new ArrayList<>();

        for (String word : text) {
            try {
                if (word.matches("\\d+")) {
                    // save digits as is
                    translation.add(word);
                } else {
                    String tr = dict.getTranslation(word);
                    if (tr.length() > 0) {
                        translation.add(tr);
                    }
                }
            } catch (NoTranslationException e) {
                translation.add(getReplacement(word));
            }
        }

        return translation;
    }

    /**
     * Create replacement string for an unknown word
     *
     * @param word unknown word
     * @return replacement string
     */
    private String getReplacement(String word) {
        return unknownWordReplacement + "(" + word + ")";
    }
}
