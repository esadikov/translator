package com.db;

import com.db.exceptions.TranslationSaveException;

import java.io.*;
import java.util.List;
import java.util.Objects;

/**
 * Translation writer class.
 *
 * @author Evgeny Sadikov
 */
public class TranslationWriter {

    /**
     * Save translation to a file
     *
     * @param translation list of translated words
     * @param fileName name of a file
     * @throws TranslationSaveException in case destination file name is incorrect
     * @throws NullPointerException in case any parameter is null
     */
    static public void writeTranslation(List<String> translation, String fileName) throws TranslationSaveException{
        Objects.requireNonNull(translation);
        Objects.requireNonNull(fileName);

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"))) {
            for (String str : translation) {
                writer.write(str + " ");
            }
        } catch (FileNotFoundException e) {
            throw new TranslationSaveException("No such file", e);
        } catch (IOException e) {
            throw new TranslationSaveException("IO error", e);
        }
    }
}
