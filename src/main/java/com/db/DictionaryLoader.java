package com.db;

import com.db.exceptions.IncorrectDictionaryFileException;

import java.io.*;
import java.util.Objects;

/**
 * Dictionary loader class.
 *
 * @author Evgeny Sadikov
 */
public class DictionaryLoader {
    private static final String WORD_SEPARATOR = "=";

    /**
     * Load the dictionary from a file.
     * File structure is a word pair per line separated with '=', e.g.
     * word=слово
     * Empty word is not allowed, empty translation is allowed ( e.g. the=).
     *
     * @param fileName name of the file with dictionary data
     * @return Created dictionary object
     * @throws IncorrectDictionaryFileException in case dictionary file is incorrect
     * @throws NullPointerException if any of parameters is null
     */
    public static Dictionary loadFromFile(String fileName) throws IncorrectDictionaryFileException {
        Objects.requireNonNull(fileName);

        Dictionary dict = new Dictionary();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"))) {
            String line;
            while((line = reader.readLine()) != null) {
                String[] words = line.split(WORD_SEPARATOR);
                if (words.length == 2) {
                    if (words[0].length() > 0) {
                        dict.addTranslation(words[0], words[1]);
                    }
                } else if (words.length == 1 && !line.startsWith(WORD_SEPARATOR)) {
                    dict.addTranslation(words[0], "");
                }
            }
        } catch (FileNotFoundException e) {
            throw new IncorrectDictionaryFileException("No such file", e);
        } catch (IOException e) {
            throw new IncorrectDictionaryFileException("IO error", e);
        }
        return dict;
    }

}
