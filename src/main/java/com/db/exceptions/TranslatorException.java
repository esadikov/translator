package com.db.exceptions;

/**
 * Root of application exceptions.
 *
 * @author Evgeny Sadikov
 */
public class TranslatorException extends Exception {
    public TranslatorException() {
        super();
    }

    public TranslatorException(String message) {
        super(message);
    }

    public TranslatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public TranslatorException(Throwable cause) {
        super(cause);
    }

    protected TranslatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
