package com.db.exceptions;

/**
 * Incorrect dictionary file exception
 *
 * @author Evgeny Sadikov
 */
public class IncorrectDictionaryFileException extends TranslatorException {
    public IncorrectDictionaryFileException() {
        super();
    }

    public IncorrectDictionaryFileException(String message) {
        super(message);
    }

    public IncorrectDictionaryFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectDictionaryFileException(Throwable cause) {
        super(cause);
    }

    protected IncorrectDictionaryFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
