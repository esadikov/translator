package com.db.exceptions;

/**
 * Text load exception
 *
 * @author Evgeny Sadikov
 */
public class TextLoadException extends TranslatorException {
    public TextLoadException() {
        super();
    }

    public TextLoadException(String message) {
        super(message);
    }

    public TextLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public TextLoadException(Throwable cause) {
        super(cause);
    }

    protected TextLoadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
