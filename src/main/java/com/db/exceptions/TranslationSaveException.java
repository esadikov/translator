package com.db.exceptions;

/**
 * Translation save exception
 *
 * @author Evgeny Sadikov
 */
public class TranslationSaveException extends TranslatorException {
    public TranslationSaveException() {
        super();
    }

    public TranslationSaveException(String message) {
        super(message);
    }

    public TranslationSaveException(String message, Throwable cause) {
        super(message, cause);
    }

    public TranslationSaveException(Throwable cause) {
        super(cause);
    }

    protected TranslationSaveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
