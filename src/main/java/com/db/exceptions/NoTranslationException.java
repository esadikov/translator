package com.db.exceptions;

/**
 * No translation exception
 *
 * @author Evgeny Sadikov
 */
public class NoTranslationException extends TranslatorException {
    public NoTranslationException() {
        super();
    }

    public NoTranslationException(String message) {
        super(message);
    }

    public NoTranslationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoTranslationException(Throwable cause) {
        super(cause);
    }

    protected NoTranslationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
