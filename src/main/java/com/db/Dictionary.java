package com.db;

import com.db.exceptions.NoTranslationException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Dictionary class.
 *
 * @author Evgeny Sadikov
 */
public class Dictionary {
    private final Map<String, String> wordMap;

    public Dictionary() {
        wordMap = new HashMap<>();
    }

    /**
     * Adds a translation of a word to the dictionary
     *
     * @param word a word to add
     * @param translation a translation of the word
     *
     * @throws NullPointerException if any of parameters is null
     */
    public void addTranslation(String word, String translation) {
        Objects.requireNonNull(word);
        Objects.requireNonNull(translation);

        wordMap.put(word.toLowerCase(), translation.toLowerCase());
    }

    /**
     * Looks for a translation of a word in the dictionary
     *
     * @param word a word to find translation
     * @return translation of a word
     *
     * @throws com.db.exceptions.NoTranslationException if translation wasn't found
     * @throws NullPointerException if any of parameters is null
     */
    public String getTranslation(String word) throws NoTranslationException {
        Objects.requireNonNull(word);

        String translation = wordMap.get(word.toLowerCase());
        if (null == translation) {
            throw new NoTranslationException("No translation for word \"" + word + "\"");
        }

        return translation;
    }
}
